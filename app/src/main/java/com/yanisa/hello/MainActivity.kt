package com.yanisa.hello

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }
    val TAG="Hello:"
    fun BMI2(view: View?) {
        val name:TextView = findViewById(R.id.textView2)
        val id:TextView = findViewById(R.id.textView4)
        Log.d(TAG, "name: ${name.text} | stuid: ${id.text}")
        val i = Intent(this, MainActivity2::class.java)
        i.putExtra("name",name.text)
        startActivity(i)
    }
}