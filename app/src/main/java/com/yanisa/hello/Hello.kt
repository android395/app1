package com.yanisa.hello

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class Hello :AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.hello_activity)

        val  displayName: TextView=findViewById(R.id.textView3)
        val name: String=intent.getStringExtra("name").toString()
        displayName.text=name

    }

}